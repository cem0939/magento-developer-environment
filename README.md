# Magento-Developer-Environment

**The requirements of this environment are:**

- VirtualBox (>=5.0.*)
- Vagrant (>=1.8.7)
- Vagrant Plugins:
    - vagrant-proxyconf (>=1.5.2)
    - vagrant-cachier (>=1.2.1)
    - vagrant-timezone (>=1.2.0)
- Box [PuppetLabs/Debian-8.2-64-Puppet (virtualbox, 1.0.1)](https://atlas.hashicorp.com/puppetlabs/boxes/debian-8.2-64-puppet) 

**This environment has:**

- PHP v5.6 ( with Zend OPcache v7.0.6-dev and Xdebug v2.4.0 )
- Apache v2.4.10
- MySQL Server v5.5

**Environment variables:**

````
# It's defined in Vagrantfile
...
puppet.facter = {
  "servername"      => 'server.magento.local',
  "serveraliases"   => 'web.magento.local',
  "redirect"        => 'false',
  "db_user"         => 'magento_user',
  "db_password"     =>'123456789',
  "db_name"         => 'ecommerce',
  "db_driver"       => 'pdo_mysql',
  "db_host"         => 'localhost',
  "db_port"         => '3306',
  "debug"           => 'true'
}
...
````

### Get Started

**Step 1: Set Local Configuration Proxy**
```
$ set HTTPS_PROXY=http://spobrproxy:3128
$ set HTTP_PROXY=http://spobrproxy:3128
$ set http_proxy=http://spobrproxy:3128
$ set https_proxy=http://spobrproxy:3128
```

**Step 2: Installing required plugins**

````
$ vagrant plugin install vagrant-proxyconf vagrant-cachier vagrant-timezone
````

**Step 3: Run the server with the following command**
````
$ vagrant up
````

### Configure your file /etc/hosts

**Linux:**
````
$ echo '127.0.0.1 server.magento.local' >> /etc/hosts
````

**Windows:**
````
$ echo '127.0.0.1 server.magento.local' >> /c/Windows/System32/drivers/etc/hosts
````

### in Your Browser

````
http://server.magento.local:8080 ou http://localhost:8080
````

### How to Debugging (Remotely)

- **[with PHPStorm](https://confluence.jetbrains.com/display/PhpStorm/Configuring+a+Vagrant+VM+for+Debugging)**
- **[with Netbeans](https://netbeans.org/kb/docs/php/debugging.html)**
- **[with Eclipse](https://wiki.eclipse.org/Debugging_using_XDebug)**
