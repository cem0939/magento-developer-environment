
class utils {

  exec{'fixed':
    command => 'apt-get update --fix-missing -y',
    path    => ['/usr/bin', '/usr/sbin',]
  }

  $basic_packages = [
    'vim','git','gem','zip','build-essential','curl','iptables-persistent','figlet','htop'
  ]

  package{$basic_packages:
    ensure => installed,
    require => Exec['fixed']
  }

  exec{'welcome':
    command => 'figlet Welcome Magento Server > /etc/motd && echo "by Andre Vitor Cuba de Miranda <andre.miranda@gft.com>" >> /etc/motd',
    path    => ['/usr/bin', '/usr/sbin']
  }
}

class accounts {
  user { 'vagrant':
    ensure   => present,
    password => '$1$ImQjhUIS$oG7ejFp/tReDLujM2nkE80',
  }
}

class webserver{

  class {
    'apache':
      default_vhost => false,
      mpm_module => false,
      default_charset => 'utf-8'
  }

  class {
    'apache::mod::prefork':
      startservers    => "5",
      minspareservers => "3",
      maxspareservers => "3",
      serverlimit     => "64",
      maxclients      => "64",
  }

  class {
    'apache::mod::expires':
      expires_default => 'access plus 1 year'
  }

  include apache::mod::php
  include apache::mod::alias
  include apache::mod::rewrite
  include apache::mod::ssl
  include apache::mod::vhost_alias

  if $redirect == 'true' {
    apache::vhost {  "redirect.${servername} non-ssl":
      servername      => $servername,                                 #	Dominio definido para o site
      serveraliases   => $serveraliases,                              # Dominio alias ou sub dominios usados como aliases
      port            => '80',                                        # Porta que o Site está ouvindo no Apache
      docroot         => '/var/www/redirect',                         # Diretório Raiz do Site
      redirect_status => 'permanent',                                 # 301 Movido Permanentemente
      redirect_dest   => "https://${servername}/",                    # Destino do Redirect
      priority		    => 15,                                          # Prioridade do Site no Apache
    }
  } else {
    apache::vhost { "${servername} non-ssl":
      servername      => $servername,                                 #	Dominio definido para o site
      serveraliases   => $serveraliases,                              # Dominio alias ou sub dominios usados como aliases
      setenv          => [
        "APPLICATION_DEBUG ${debug}",
        "APPLICATION_DB_DRIVE ${db_driver}",
        "APPLICATION_DB_NAME ${db_name}",
        "APPLICATION_DB_HOST ${db_host}",
        "APPLICATION_DB_PORT ${db_port}",
        "APPLICATION_DB_USER ${db_user}",
        "APPLICATION_DB_PASSWORD ${db_password}",
      ],                                                              # Variaveis de Ambientes
      priority		    => 15,										  							      # Prioridade do Site no Apache
      port          	=> '80',								  								      # Porta que o Site está ouvindo no Apache
      ssl             => false,
      docroot       	=> '/var/www/web/',						  					      # Diretório Raiz do Site
      directories     => [{
        path 			      => '/var/www/web/',		  											# Diretório Raiz. Definido no Vagrantfile (server.vm.synced_folder)
        directoryindex  => 'index.php index.html',								    # Indice do Diretorio.
        options 	      => ['Indexes','FollowSymLinks','MultiViews'],	# Opções de Visualização de Conteúdo
        allow_override => [ 'all' ],                                  # Permite override a partir de um .htaccess
      }],
      docroot_owner 	=> 'www-data',														      # Usuário Dono da Pasta ( Usuario de Serviço no Linux )
      docroot_group 	=> 'www-data',														      # Grupo Dono da Pasta
      access_log_file => "${servername}-access.log",                  # Logs de acesso
      error_log_file  => "${servername}-error.log",                   # Logs de erros
    }
  }

  apache::vhost { "${servername} ssl":
    servername      => $servername,                                 #	Dominio definido para o site
    serveraliases   => $serveraliases,                              # Dominio alias ou sub dominios usados como aliases
    setenv          => [
      "APPLICATION_DEBUG ${debug}",
      "APPLICATION_DB_DRIVE ${db_driver}",
      "APPLICATION_DB_NAME ${db_name}",
      "APPLICATION_DB_HOST ${db_host}",
      "APPLICATION_DB_PORT ${db_port}",
      "APPLICATION_DB_USER ${db_user}",
      "APPLICATION_DB_PASSWORD ${db_password}",
    ],                                                              # Variaveis de Ambientes
    priority		    => 15,										  							      # Prioridade do Site no Apache
    port          	=> '443',								  								      # Porta que o Site está ouvindo no Apache
    ssl             => true,
    docroot       	=> '/var/www/web/',						  					      # Diretório Raiz do Site
    directories     => [{
      path 			      => '/var/www/web/',		  											# Diretório Raiz. Definido no Vagrantfile (server.vm.synced_folder)
      directoryindex  => 'index.php index.html',								    # Indice do Diretorio.
      options 	      => ['Indexes','FollowSymLinks','MultiViews'],	# Opções de Visualização de Conteúdo
      allow_override => [ 'all' ],                                  # Permite override a partir de um .htaccess
    }],
    docroot_owner 	=> 'www-data',														      # Usuário Dono da Pasta ( Usuario de Serviço no Linux )
    docroot_group 	=> 'www-data',														      # Grupo Dono da Pasta
    access_log_file => "${servername}-access-ssl.log",              # Logs de acesso
    error_log_file  => "${servername}-error-ssl.log",               # Logs de erros
  }

  firewall { '100 allow http and https access':
    dport  => [80, 443],
    proto  => tcp,
    action => accept,
  }
}

class language{

  class{'::php':
    #ensure       => latest,
    manage_repos => true,
    fpm          => false,
    dev          => true,
    composer     => true,
    pear         => true,
    phpunit      => false,
    settings   => {
      'PHP/max_execution_time'  => 90,
      'PHP/magic_quotes_gpc'    => false,
      'PHP/display_errors'      => true,
      'PHP/log_errors'          => true,
      'PHP/track_errors'        => true,
      'PHP/html_errors'         => true,
      'PHP/max_input_time'      => 300,
      'PHP/memory_limit'        => '256M',
      'PHP/post_max_size'       => '32M',
      'PHP/upload_max_filesize' => '32M',
      'PHP/session.auto_start'  => false,
      'Date/date.timezone'      => 'UTC',
    },
    extensions => {
      curl => {},
      gd => {},
      mcrypt => {},
      mysql => {},
      memcached => {},
      oauth => {},
      curl => {},
      json => {},
      xdebug => {},
      xsl => {},
      xmlrpc => {},
    },
    notify  => Service['apache2'],
  } ->
  file { '/etc/php5/mods-available/xdebug.ini':
    ensure  => 'file',
    source  => '/vagrant/puppet/files/xdebug.ini',
    notify  => Service['apache2'],
  }

}

class database {

  class { '::mysql::server':
    root_password           => 'account-will-be-disabled',          # Não importa qual a senha do root, pois será desabilitada.
    remove_default_accounts => true,                                # Desabilita as contas padrão ( root ).
    override_options        => {
      'mysqld' => {
        port                => 3306,
        connect_timeout     => 60,
        bind_address        => '0.0.0.0',
        max_connections     => 100,
        max_allowed_packet  => '128M',
        thread_cache_size   => 16,
        query_cache_size    => '128M',
        pid-file            => '/var/run/mysqld/mysqld.pid',
        general-log         => true,
        general-log-file    => '/var/log/mysql/mysql.log',
        log-error           => '/var/log/mysql/mysql-err.log',
        slow-query-log      => true,
        slow-query-log-file => '/var/log/mysql/mysql-slow.log'
      }
    },
  }

  mysql::db { $db_name:
    user      => $db_user,
    password  => $db_password,
    host      => '%',
    grant     => ['all'],
    ensure    => present,
    charset   => 'utf8',
    collate   => 'utf8_general_ci',
    require   => Class['mysql::server'],
  }

  firewall { '101 allow mysql access':
    dport  => [3306],
    proto  => tcp,
    action => accept,
  }
}

node "server.magento.local" {

  include utils
  include accounts
  include webserver
  include language
  include database

  Class['utils'] -> Class['accounts']
  Class['accounts'] -> Class['webserver']
  Class['webserver'] -> Class['language']
  Class['webserver'] -> Class['database']

}
