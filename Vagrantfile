# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
    config.vm.box = "puppetlabs/debian-8.2-64-puppet"
    config.vm.box_check_update = false

    # Vagrant Plugin ProxyConf
    config.proxy.http       = "http://spobrproxy:3128"
    config.proxy.https      = "http://spobrproxy:3128"
    config.proxy.no_proxy   = "localhost,127.0.0.1,lab.magento.local,db.magento.local"
    config.apt_proxy.http   = "http://spobrproxy:3128"
    config.apt_proxy.https  = "http://spobrproxy:3128"

    # Vagrant Plugin Cachier
    config.cache.scope = :machine
    config.cache.auto_detect = true
    config.cache.enable :apt
    config.cache.enable :gem

    # Vagrant Plugin TimeZone
    config.timezone.value = "UTC"

    config.vm.define "server" do |server|
        server.vm.network :forwarded_port, guest: 80, host: 8080
        server.vm.network :forwarded_port, guest: 443, host: 443
        server.vm.network :forwarded_port, guest: 3306, host: 3306
        server.vm.network :forwarded_port, guest: 9000, host: 9080
        server.vm.hostname = "server.magento.local"
        server.vm.synced_folder "source/magento-1.7.0.2-2015-02-11-09-59-20/", "/var/www/web/"
        server.vm.synced_folder "puppet/modules/", "/tmp/vagrant-puppet/modules/"

        server.vm.provider :virtualbox do |v|
            v.customize [
                "modifyvm",:id,
                "--memory",1024,
                "--cpus",1,
                "--name","server",
                "--groups","/Magento-Develop"
            ]
        end
    end

    config.vm.provision :shell, inline: <<-SHELL
        puppet module install puppetlabs-apache --version 1.11.0 --target-dir /tmp/vagrant-puppet/modules
        puppet module install puppetlabs-mysql --version 3.11.0 --target-dir /tmp/vagrant-puppet/modules
        puppet module install puppet-php --version 4.0.0 --target-dir /tmp/vagrant-puppet/modules
        puppet module install puppetlabs-firewall --version 1.9.0 --target-dir /tmp/vagrant-puppet/modules
    SHELL

    config.vm.provision :puppet do |puppet|
        puppet.environment_path = "puppet/environments"
        puppet.environment = "develop"
        puppet.module_path = "puppet/modules"
        #puppet.options = ['--verbose', '--debug','--disable_warnings=deprecations']
        puppet.options = ['--disable_warnings=deprecations']
        puppet.facter = {
          "servername"  => 'server.magento.local',
          "serveraliases" => 'web.magento.local',
          "redirect" => 'false',
          "db_user" => 'magento_user',
          "db_password" =>'123456789',
          "db_name" => 'ecommerce',
          "db_driver" => 'pdo_mysql',
          "db_host" => 'localhost',
          "db_port" => '3306',
          "debug"    => 'true'
        }
    end
end
