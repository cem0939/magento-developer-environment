# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## 0.1.0 - 2017-06-17
### Added
- This changelog file with settings based on [Keep a Changelog](http://keepachangelog.com/) and [Semantic Versioning](http://semver.org/)
- Included Magento Package v1.7.0.2
- Provision with Puppet ( PHP5.6 [ Composer and Xdebug ] , MySQL 5.5, Apache 2.4 )
- Vagrantfile + Puppet + Plugins
